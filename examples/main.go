package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/it-sgn/gozk"
)

func main() {
	zkSocket := gozk.NewZK("192.168.50.32", 4370, 0, gozk.DefaultTimezone, true)
	if err := zkSocket.Connect(); err != nil {
		panic(err)
	}

	c, err := zkSocket.LiveCapture()
	if err != nil {
		panic(err)
	}

	go func() {
		for event := range c {
			log.Println(event)

		}
	}()

	gracefulQuit(zkSocket.StopCapture)
}

func gracefulQuit(f func()) {
	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-sigChan

		log.Println("Stopping...")
		f()

		time.Sleep(time.Second * 1)
		os.Exit(1)
	}()

	for {
		time.Sleep(10 * time.Second) // or runtime.Gosched() or similar per @misterbee
	}
}

func ReverseHex(hex string) string {
	var data string
	for i := len(hex)/2 - 1; i >= 0; i-- {
		start := i * 2
		end := (i * 2) + 2
		data += hex[start:end]
	}
	return data
}
